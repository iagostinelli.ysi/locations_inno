class Province < ApplicationRecord
  has_many :municipalities
  
  belongs_to :country

  validates :name, presence: true

  scope :by_name, lambda { |name| where("LOWER(name) LIKE ? ", "%#{name.downcase}%") }

end
