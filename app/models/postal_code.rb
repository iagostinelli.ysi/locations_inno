class PostalCode < ApplicationRecord
  belongs_to :municipality

  validates :code, presence: true, length: {in: 4..5}
  
  scope :code, lambda { |code| where("LOWER(code) LIKE ? ", "#{code.downcase}%") }

end
