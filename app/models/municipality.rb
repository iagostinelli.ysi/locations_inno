class Municipality < ApplicationRecord
  has_many :postal_codes
  
  belongs_to :province

  validates :name, presence: true

  scope :by_name, lambda { |name| where("LOWER(name) LIKE ? ", "%#{name.downcase}%") }
  scope :by_postal_code, lambda { |code| joins(:postal_codes).where("LOWER(postal_codes.code) LIKE ? ", "#{code.downcase}%")}


end
