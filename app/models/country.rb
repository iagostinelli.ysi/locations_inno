class Country < ApplicationRecord
  has_many :provinces

  validates :name, presence: true
  # validates :code, presence: true

  scope :by_name, lambda { |name| where("LOWER(name) LIKE ? ", "%#{name.downcase}%") }

end
