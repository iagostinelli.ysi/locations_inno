class CountriesController < ApplicationController
  before_action :set_country, only: [:show, :update, :destroy, :country_provinces]

  # GET /countries
  def index
    @countries = Country.all
    @countries = @countries.by_name(params[:name]) if params[:name].present?

    render json: @countries
  end

  # GET /countries/1
  def show
    render json: @country
  end

  # POST /countries
  def create
    @country = Country.new(country_params)

    if @country.save
      render json: @country, status: :created, location: @country
    else
      render json: @country.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /countries/1
  def update
    if @country.update(country_params)
      render json: @country
    else
      render json: @country.errors, status: :unprocessable_entity
    end
  end

  # DELETE /countries/1
  def destroy
    @country.destroy
  end

  #GET /country/:id/provinces
  def country_provinces
    @provinces = @country.provinces
    render json: @provinces
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_country
      @country = Country.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def country_params
      params.require(:country).permit(:name, :code)
    end
end
