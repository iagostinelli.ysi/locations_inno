class MunicipalitiesController < ApplicationController
  before_action :set_municipality, only: [:show, :update, :destroy, :municipality_postal_codes]

  # GET /municipalities
  def index
    @municipalities = Municipality.all
    @municipalities = @municipalities.by_name(params[:name]) if params[:name].present?
    @municipalities = @municipalities.by_postal_code(params[:postal_code]) if params[:postal_code].present?

    render json: @municipalities
  end

  # GET /municipalities/1
  def show
    render json: @municipality
  end

  # POST /municipalities
  def create
    @municipality = Municipality.new(municipality_params)

    if @municipality.save
      render json: @municipality, status: :created, location: @municipality
    else
      render json: @municipality.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /municipalities/1
  def update
    if @municipality.update(municipality_params)
      render json: @municipality
    else
      render json: @municipality.errors, status: :unprocessable_entity
    end
  end

  # DELETE /municipalities/1
  def destroy
    @municipality.destroy
  end

  #GET /province/:id/municipalities
  def municipality_postal_codes
    @postal_codes = @municipality.postal_codes
    render json: @postal_codes
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_municipality
      @municipality = Municipality.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def municipality_params
      params.require(:municipality).permit(:name, :province_id)
    end
end
