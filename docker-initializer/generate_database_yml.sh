#!/bin/bash
cat << EOF
defaults: &defaults
  adapter: postgresql
  encoding: utf8
  sslrootcert: $DB_SSLROOTCERT
  pool: 5
production:
  <<: *defaults
  url: $DB_URL
EOF