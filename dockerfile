FROM ruby:2.5.3-slim

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

ENV RAILS_ROOT /locations

RUN mkdir $RAILS_ROOT

WORKDIR $RAILS_ROOT

COPY Gemfile $RAILS_ROOT/Gemfile

COPY Gemfile.lock $RAILS_ROOT/Gemfile.lock

RUN gem install bundler -v 2.0.1

RUN bundle install

COPY . $RAILS_ROOT

RUN ["chmod", "+x", "/locations/config/docker-entrypoint.sh"]

ENTRYPOINT ["/locations/config/docker-entrypoint.sh"]

CMD RAILS_ENV=production bundle exec rails s -p 8080 -b '0.0.0.0'
