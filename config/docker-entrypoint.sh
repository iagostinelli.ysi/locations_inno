#!/bin/bash
# stop execution if any commands fail
set -e

# define env vars, this becomes the container interface definition file
# source /locations/docker-initializers/check_env_vars.sh


# generate database.yml
source /locations/docker-initializers/generate_database_yml.sh > /locations/config/database.yml

# run command from either CMD instruction or docker run
exec "$@"