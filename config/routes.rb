Rails.application.routes.draw do
  resources :postal_codes
  resources :municipalities
  resources :provinces
  resources :countries

  get '/country/:id/provinces' => "countries#country_provinces", as: :country_provinces
  get '/province/:id/municipalities' => "province#province_municipalities", as: :province_municipalities
  get '/municipality/:id/postal_codes' => "municipality#municipality_postal_codes", as: :municipality_postal_codes


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
